# Gateway

## About
The reverse proxy written in C# with use of Ocelote library which is the only entrance to the system for outside of the infrastructure. The authorization of the JWT token is happening there on enterance to the gateway. The main file ocelot.json consists all definitions for proxying the requests from outside the infrastrucutre.

## Ocelot configuration example

```
{
  "Routes": [
    {
      "DownstreamPathTemplate": "/{everything}",
      "DownstreamScheme": "http",
      "DownstreamHostAndPorts": [
        {
          "Host": "auth",
          "Port": 80
        }
      ],
      "UpstreamPathTemplate": "/auth/{everything}",
      "UpstreamHttpMethod": [ "Get" ]
    },
    {
      "DownstreamPathTemplate": "/{everything}",
      "DownstreamScheme": "http",
      "DownstreamHostAndPorts": [
        {
          "Host": "gamelib",
          "Port": 8080
        }
      ],
      "UpstreamPathTemplate": "/steam-api/{everything}",
      "UpstreamHttpMethod": [ "Get" ],
      "AuthenticationOptions": {
        "AuthenticationProviderKey": "Gateway",
        "AllowedScopes": []
      }
    }
  ],
  "GlobalConfiguration": {
    "BaseUrl": "http://localhost:2000"
  }
}
```

The `/auth` route is not protected because it's being used for authentication  
The `/steam-api` route is being protected what means that requetes made on that route must include valid JWT token to be proxied to the gamelib  API.

## How to run
`docker build -t gateway .`  
`docker run -p 8080:80 gateway`

To run whole steamx application check `Infrastructure` repository